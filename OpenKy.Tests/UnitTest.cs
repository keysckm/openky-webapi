﻿using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Concrete;
using OpenKy.Domain.Entities;

namespace OpenKy.Tests
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void PasswordCryptoTest()
        {
            var result = Util.PasswordCrypt("asdf");
            Assert.AreEqual("3da541559918a808c2402bba5012f6c60b27661c", result);
        }
    }
}
