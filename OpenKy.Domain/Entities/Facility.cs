namespace OpenKy.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Facility")]
    public partial class Facility
    {
        public int Id { get; set; }

        public int? Floor { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public int Building { get; set; }

        [Required]
        [StringLength(20)]
        public string Manager { get; set; }

        public bool? IsLock { get; set; }

        [StringLength(100)]
        public string Device { get; set; }
    }
}
