﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenKy.Domain.Entities
{
    [Table("UseApplication")]
    public partial class UseApplication
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Number { get; set; }

        public int Class { get; set; }

        [Column(TypeName = "date")]
        public DateTime UseDate { get; set; }

        [Required]
        [StringLength(50)]
        public string Period { get; set; }
        
        [Required]
        [StringLength(100)]
        public string Purpose { get; set; }

        public int Peoples { get; set; }

        public bool? Approval { get; set; }

        public DateTime ApplicationDate { get; set; }
    }
}
