namespace OpenKy.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TimeTable")]
    public partial class TimeTable
    {
        public int Id { get; set; }

        public int Year { get; set; }

        public int Term { get; set; }

        [Required]
        [StringLength(20)]
        public string Course { get; set; }

        public int Class { get; set; }

        public int Weekday { get; set; }

        [Required]
        [StringLength(50)]
        public string Period { get; set; }
    }
}
