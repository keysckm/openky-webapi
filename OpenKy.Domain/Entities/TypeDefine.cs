﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenKy.Domain.Entities
{
    [Table("TypeDefine")]
    public partial class TypeDefine
    {
        [Key]
        public int Type { get; set; }
        
        [Required]
        [StringLength(30)]
        public string Name { get; set;}
    }
}
