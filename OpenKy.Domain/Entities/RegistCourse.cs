﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenKy.Domain.Entities
{

    [Table("RegistCourse")]
    public class RegistCourse
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Number { get; set; }

        public int Year { get; set; }

        public int Term { get; set; }

        [Required]
        [StringLength(20)]
        public string Course { get; set; }
    }
}
