﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenKy.Domain.Entities
{
    [Table("UseLog")]
    public class UseLog
    {
        public int Id { get; set; }

        public int Facility { get; set; }

        [Required]
        [StringLength(20)]
        public string UnlockUser { get; set; }

        [StringLength(20)]
        public string LockUser { get; set; }

        [Required]
        public DateTime UnlockDate { get; set; }

        public DateTime? LockDate { get; set; }
    }
}
