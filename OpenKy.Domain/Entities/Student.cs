﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenKy.Domain.Entities
{
    [Table("Student")]
    public class Student
    {
        [Key]
        [StringLength(20)]
        public string Number { get; set; }

        [StringLength(20)]
        public string Professor { get; set; }
    }
}
