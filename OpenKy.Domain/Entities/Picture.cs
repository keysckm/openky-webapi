namespace OpenKy.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Picture")]
    public partial class Picture
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        [StringLength(20)]
        public string Number { get; set; }

        [Required]
        public byte[] Data { get; set; }

        [Required]
        [StringLength(50)]
        public string Mimetype { get; set; }
    }
}
