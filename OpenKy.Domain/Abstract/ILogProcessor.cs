﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    public interface ILogProcessor
    {
        /// <summary>
        /// 시설물 로그를 가져옵니다.
        /// 단, 아직 완료되지 않은 로그(열린 기록은 있으나 닫힌 기록은 없는 로그)는 제외합니다.
        /// </summary>
        /// <param name="fid">로그 데이터를 가져올 시설물의 id입니다.</param>
        /// <param name="date">로그 데이터를 가져올 날짜입니다. 시설물이 Unlock된 시간 기준입니다.</param>
        /// <returns>UseLog의 IQueryable입니다.</returns>
        IQueryable<UseLog> GetLogs(int fid, DateTime date);
    }
}
