﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    public interface ITimetableProcessor
    {
        /// <summary>
        /// TimeTable 테이블의 모든 정보를 가져옵니다.
        /// </summary>
        /// <returns>TimeTable의 IQueryable입니다.</returns>
        IQueryable<TimeTable> GetTimeTables();

        /// <summary>
        /// 특정 시설물의 특정 요일의 정규 수업 정보를 가져옵니다.
        /// </summary>
        /// <param name="classid">가져올 시설물입니다.</param>
        /// <param name="weekday">요일입니다. 1:월 ~ 7:일</param>
        /// <returns>TimeTable의 IQuerayble입니다.</returns>
        IQueryable<TimeTable> GetTimeTables(int classid, int weekday);

        /// <summary>
        /// 교시가 겹치는지 확인합니다.
        /// </summary>
        /// <param name="year">년</param>
        /// <param name="term">학기</param>
        /// <param name="weekday">요일</param>
        /// <param name="period">콤마로 구분된 교시 문자열</param>
        /// <returns>겹치면 true, 그렇지 않으면 false</returns>
        bool IsOverlap(int @class, int year, int term, int weekday, string period);

        TimeTable AddTimeTable(TimeTable data);
        TimeTable UpdateTimeTable(int id, TimeTable data);
        TimeTable DeleteTimeTable(int id);
    }
}
