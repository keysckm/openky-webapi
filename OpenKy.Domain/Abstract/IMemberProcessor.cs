﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Concrete;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    public interface IMemberProcessor
    {
        IQueryable<Member> GetMembers();
        IQueryable<Member> GetMembers(int type);
        Member GetMember(string number);

        bool AddMember(Member data);
        bool UpdateMember(string number, Member data);
        Member DeleteMember(string number);

        Member FindToken(string token);

        /// <summary>
        /// GCM ID를 등록합니다.
        /// </summary>
        /// <param name="member">Member 객체입니다.</param>
        /// <param name="regid">등록될 GCM ID입니다.</param>
        /// <returns>성공시 true, 실패시 false를 반환합니다.</returns>
        bool AttachGcmId(Member member, string regid);

        /// <summary>
        /// GCM ID를 해제합니다.
        /// </summary>
        /// <param name="member">GCM ID를 해제할 Member 데이터.</param>
        /// <returns></returns>
        bool DetachGcmId(Member member);


        /// <summary>
        /// GCM ID를 받아옵니다.
        /// </summary>
        /// <param name="number">GCM ID를 받아올 Member테이블의 Number입니다.</param>
        /// <returns>성공시 GCM ID, 실패시 ""를 반환합니다.</returns>
        string GetGcmId(string number);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="member"></param>
        /// <param name="regid"></param>
        /// <returns></returns>
        bool AttachToken(Member member, string token);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        bool DetachToken(Member member);
    }
}
