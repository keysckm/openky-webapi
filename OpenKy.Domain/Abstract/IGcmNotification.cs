﻿using System.Collections.Generic;
namespace OpenKy.Domain.Abstract
{
    public interface IGcmNotification
    {
        /// <summary>
        /// GCM Notification을 Push합니다.
        /// data.key = value 데이터쌍으로 보냅니다.
        /// </summary>
        /// <param name="number">Notification을 수신할 자의 학번/교번입니다.</param>
        /// <param name="datas">key=data 데이터쌍입니다.</param>
        /// <returns>성공시 true, 실패시 false를 반환합니다.</returns>
        bool Push(string number, Dictionary<string, string> datas);

        /// <summary>
        /// GCM Notification을 Push합니다.
        /// </summary>
        /// <param name="number">Notification을 수신할 자의 학번/교번입니다.</param>
        /// <param name="msg">보낼 메시지입니다.</param>
        /// <returns>성공시 true, 실패시 false를 반환합니다.</returns>
        bool Push(string number, string msg);
    }
}
