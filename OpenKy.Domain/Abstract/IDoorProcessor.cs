﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace OpenKy.Domain.Abstract
{
    public interface IDoorProcessor
    {
        /// <summary>
        /// 문을 엽니다.
        /// 관리자는 무조건 문을 열 수 있습니다.
        /// </summary>
        /// <param name="nfc">태깅된 NFC카드의 ID입니다.</param>
        /// <param name="device">디바이스의 ID입니다.</param>
        /// <param name="reqTime">태깅된 시각입니다.</param>
        /// <returns>성공시 true, 권한이 없는 경우 false를 반환합니다.</returns>
        bool Unlock(string nfc, string device, DateTime reqTime);

        /// <summary>
        /// 문을 잠급니다.
        /// 학생이라면 문을 열었던 사람만이 문을 닫을 수 있습니다.
        /// 관리자는 무조건 문을 닫을 수 있습니다.
        /// </summary>
        /// <param name="nfc">태깅된 NFC카드의 ID입니다.</param>
        /// <param name="device">디바이스의 ID입니다.</param>
        /// <param name="reqTime">태깅된 시각입니다.</param>
        /// <returns>성공시 true, 권한이 없는 경우 false를 반환합니다.</returns>
        bool Lock(string nfc, string device, DateTime reqTime);
    }
}
