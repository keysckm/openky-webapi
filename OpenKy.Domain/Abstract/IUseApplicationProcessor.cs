﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    public interface IUseApplicationProcessor
    {
        /// <summary>
        /// 시설물 사용 신청서를 제출합니다.
        /// </summary>
        /// <param name="rq">사용 신청서 데이터입니다.</param>
        /// <returns>등록 성공시 true, 그렇지 않은 경우 false를 반환합니다.</returns>
        bool Request(UseApplication rq);

        /// <summary>
        /// 시설물 사용 신청을 취소합니다.
        /// </summary>
        /// <param name="id">신청을 취소할 신청서의 index입니다.</param>
        /// <returns>성공시 true, 신청서를 찾지 못하였을 경우 false를 반환합니다.</returns>
        bool Cancel(int id);

        /// <summary>
        /// 시설물 사용 신청을 승인합니다.
        /// </summary>
        /// <param name="id">승인할 신청서의 index입니다.</param>
        /// <param name="number">신청서를 승인하려는 유저의 학번입니다.</param>
        /// <returns>성공시 true, 신청서를 찾지 못했거나 권한이 없는 경우 false를 반환합니다.</returns>
        bool Accept(int id, string number);
        
        /// <summary>
        /// 시설물 사용 신청을 거부합니다.
        /// </summary>
        /// <param name="rq">거부할 신청서의 index입니다.</param>
        /// <param name="number">신청서를 거부하려는 유저의 학번입니다.</param>
        /// <returns>성공시 true, 신청서를 ㅊ 못했거나 권한이 없는 경우 false를 반환합니다.</returns>
        bool Reject(int rq, string number);

        /// <summary>
        /// 신청서를 가져옵니다.
        /// </summary>
        /// <param name="id">가져올 신청서의 index입니다.</param>
        /// <returns>성공시 UseApplication 데이터, 데이터를 찾을 수 없는 경우 null을 반환합니다.</returns>
        UseApplication GetRequest(int id);

        /// <summary>
        /// 사용 신청 목록을 가져옵니다.
        /// </summary>
        /// <returns>UseApplication의 Queryable Collection입니다.</returns>
        IQueryable<UseApplication> GetRequests();

        /// <summary>
        /// 특정 사용자의 신청서 목록을 가져옵니다.
        /// </summary>
        /// <param name="number">특정 사용자의 학번입니다.</param>
        /// <returns>UseApplication의 Queryable Collection입니다.</returns>
        IQueryable<UseApplication> GetRequests(string number);

        /// <summary>
        /// 특정 시설물에 대한 신청 목록을 가져옵니다.
        /// </summary>
        /// <param name="facility">신청 목록을 가져올 시설물의 index입니다.</param>
        /// <returns>UseApplication의 Queryable Collection입니다.</returns>
        IQueryable<UseApplication> GetRequestForClass(int facility);

        /// <summary>
        /// 특정 관리자가 관리하는 시설물에 대한 아직 가불결정되지 않은 신청 목록을 가져옵니다.
        /// </summary>
        /// <param name="number">관리자의 사번입니다.</param>
        /// <returns>UseApplication의 Queryable Collection입니다.</returns>
        IQueryable<UseApplication> GetRequestForManager(string number);

        /// <summary>
        /// 특정 교수가 담당하는 학생들의 사용신청 목록을 가져옵니다.
        /// Request 시간보다 미래 시간의 요청만을 가져옵니다.
        /// </summary>
        /// <param name="number">교수의 사번입니다.</param>
        /// <returns>UseApplication의 Queryable Collection입니다.</returns>
        IQueryable<UseApplication> GetRequestForProfessor(string number);
    }
}
