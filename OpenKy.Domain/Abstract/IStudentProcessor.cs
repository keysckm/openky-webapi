﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    public interface IStudentProcessor
    {
        IQueryable<Student> GetStudents();
        Student GetStudent(string number);

        bool AddStudent(Student student);
        bool UpdateStudent(string number, Student student);
        bool DeleteStudent(string number);
    }
}
