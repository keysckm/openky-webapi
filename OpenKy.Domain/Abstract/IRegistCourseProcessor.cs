﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    public interface IRegistCourseProcessor
    {
        IQueryable<RegistCourse> GetRegistCourses();

        IQueryable<RegistCourse> GetRegistCourses(string memberNumber);

        RegistCourse GetRegistCourse(int id);

        bool AddRegist(RegistCourse reg);

        bool UdpateRegist(int id, RegistCourse reg);

        bool DeleteRegist(RegistCourse reg);
    }
}
