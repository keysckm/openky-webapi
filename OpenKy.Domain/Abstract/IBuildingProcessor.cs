﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    /// <summary>
    /// Building 테이블 인터페이스입니다.
    /// </summary>
    public interface IBuildingProcessor
    {
        /// <summary>
        /// Building 테이블의 모든 열을 반환합니다.
        /// </summary>
        /// <returns>Building 테이블의 모든 열</returns>
        IQueryable<Building> GetBuildings();

        /// <summary>
        /// 특정 Key를 가진 Building 테이블의 데이터를 반환합니다.
        /// </summary>
        /// <param name="id">데이터의 Key입니다.</param>
        /// <returns>Building 데이터입니다. 찾을 수 없는 경우 null을 리턴합니다.</returns>
        Building GetBuilding(int id);

        /// <summary>
        /// Building 테이블에 데이터를 삽입합니다.
        /// </summary>
        /// <param name="data">삽입 될 Building 데이터입니다.</param>
        /// <returns>성공시 true, 실패시 false</returns>
        bool AddBuilding(Building data);

        /// <summary>
        /// Building 테이블의 데이터를 수정합니다.
        /// </summary>
        /// <param name="id">수정 될 데이터의 key입니다.</param>
        /// <param name="data">수정 될 Building 데이터입니다.</param>
        /// <returns>성공시 true, 실패시 false</returns>
        bool UpdateBuilding(int id, Building data);

        /// <summary>
        /// Building 테이블에 데이터를 삭제합니다.
        /// </summary>
        /// <param name="id">삭제 될 데이터의 Key입니다.</param>
        /// <returns>성공시 true, 실패시 false</returns>
        bool DeleteBuilding(int id);
    }
}
