﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    public interface ICourseProcessor
    {
        IQueryable<Course> GetCourses();
        Course GetCourse(string id);

        bool AddCourse(Course course);
        bool UpdateCourse(string id, Course course);
        bool DeleteCourse(string id);
    }
}
