﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    public interface IDepartmentProcessor
    {
        IQueryable<Department> GetDepartments();
        Department GetDepartment(string code);

        
    }
}
