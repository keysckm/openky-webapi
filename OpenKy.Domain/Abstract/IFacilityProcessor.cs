﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    public interface IFacilityProcessor
    {
        /// <summary>
        /// 시설물 리스트를 가져옵니다.
        /// </summary>
        /// <returns>Facility의 IQueryable Collection입니다.</returns>
        IQueryable<Facility> GetFacilities();

        /// <summary>
        /// 특정 건물에 있는 시설물 리스트를 가져옵니다.
        /// </summary>
        /// <param name="building">건물 데이터</param>
        /// <returns>Facility의 IQueryable Collection입니다.</returns>
        IQueryable<Facility> GetFacilities(Building building);

        /// <summary>
        /// 특정 건물의 특정 층에 있는 시설물 리스트를 가져옵니다.
        /// </summary>
        /// <param name="building">건물 데이터</param>
        /// <param name="floar">층</param>
        /// <returns>Facility의 IQueryable Collection입니다.</returns>
        IQueryable<Facility> GetFacilities(Building building, int floar);

        /// <summary>
        /// 시설물 정보를 가져옵니다.
        /// </summary>
        /// <param name="idx">시설물 인덱스</param>
        /// <returns>성공시 Facility 객체, 찾을 수 없는 경우 null입니다.</returns>
        Facility GetFacility(int idx);

        /// <summary>
        /// 시설물 정보를 가져옵니다.
        /// </summary>
        /// <param name="device">시설물의 device id</param>
        /// <returns>성공시 Facility 객체, 찾을 수 없는 경우 null입니다.</returns>
        Facility GetFacility(string device);

        /// <summary>
        /// 특정 날짜의 시설물 사용 정보를 리턴합니다.
        /// 사용 정보에는 정규과목, 예약된 사용, 리젝되지 않은 사용 신청서가 포함됩니다.
        /// </summary>
        /// <param name="idx">시설물 인덱스</param>
        /// <param name="now">사용 정보를 가져올 날짜</param>
        /// <returns>int형 ICollection입니다. int는 사용 시간(교시) 정보입니다.</returns>
        ICollection<int> GetSchedules(int idx, DateTime now);

        /// <summary>
        /// Facility 테이블에 데이터를 등록합니다.
        /// </summary>
        /// <param name="c">Facilty 데이터</param>
        /// <returns>성공시 true, 그렇지 않은 경우 false</returns>
        bool Create(Facility c);

        /// <summary>
        /// Facility 테이블의 데이터를 수정합니다.
        /// </summary>
        /// <param name="id">수정할 Facility 데이터의 index</param>
        /// <param name="c">새로운 Facility 데이터</param>
        /// <returns>성공시 true, 그렇지 않은 경우 false</returns>
        bool Update(int id, Facility c);

        /// <summary>
        /// Facility 데이터를 삭제합니다.
        /// </summary>
        /// <param name="id">Facility데이터의 index</param>
        /// <returns>성공시 true, 그렇지 않은 경우 false</returns>
        bool Delete(int id);
    }
}
