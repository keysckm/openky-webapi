﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Abstract
{
    public interface ITypeDefineProcessor
    {
        /// <summary>
        /// Define 정보를 가져옵니다.
        /// </summary>
        /// <returns>TypeDefine의 IQueryable입니다.</returns>
        IQueryable<TypeDefine> GetTypeDefs();

        /// <summary>
        /// Define 정보를 가져옵니다.
        /// </summary>
        /// <param name="id">가져올 Define 인덱스입니다.</param>
        /// <returns>TypeDefine입니다. 찾을 수 없는 경우 null입니다.</returns>
        TypeDefine GetTypeDef(int id);

        TypeDefine FindType(string typename);

        bool CreateType(TypeDefine t);
        bool UpdateType(int type, TypeDefine t);
        bool DeleteType(int type);
    }
}
