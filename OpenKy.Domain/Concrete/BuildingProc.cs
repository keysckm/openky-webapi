﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class BuildingProc : IBuildingProcessor
    {
        private Repository repo;

        public BuildingProc()
        {
            repo = new Repository();
        }

        public BuildingProc(Repository repository)
        {
            repo = repository;
        }

        public IQueryable<Building> GetBuildings()
        {
            return repo.Building;
        }

        public Building GetBuilding(int id)
        {
            return repo.Building.Find(id);
        }

        public bool AddBuilding(Building data)
        {
            repo.Building.Add(data);
            repo.SaveChanges();

            return true;
        }

        public bool UpdateBuilding(int id, Building data)
        {
            if (id != data.Id)
                return false;

            repo.Entry(data).State = EntityState.Modified;
            repo.SaveChanges();

            return true;
        }

        public bool DeleteBuilding(int id)
        {
            Building result = repo.Building.Find(id);
            if (result == null)
                return false;

            repo.Building.Remove(result);
            repo.SaveChanges();
            return true;
        }
    }
}
