﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Validation;
using System.Linq;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class DoorProc : IDoorProcessor
    {
        public Repository repo { get; private set; }

        public DoorProc()
        {
            repo = new Repository();
        }

        public DoorProc(Repository repo)
        {
            this.repo = repo;
        }

        public bool Unlock(string nfc, string device, DateTime reqTime)
        {
            try
            {
                bool isMatch = false;
                // device와 일치하는 시설물을 가져온다.
                Facility f = repo.Facility.SingleOrDefault(e => e.Device == device);
                int period = Util.TimeToPeriod(reqTime.Hour);   // 교시
                int? term = Util.MonthToTerm(reqTime.Month);    // 학기

                Nfclist nfcinfo = repo.Nfclist.SingleOrDefault(e => e.Nfc == nfc);
                Member member = repo.Member.Find(nfcinfo.Number);

                // 건물의 관리자라면 무조건 열 수 있음
                if (member.Type == 3 && member.Number == f.Manager)
                    return true;

                if (term != null)   // 수업기간이라면 정규수업 수강여부부터 조회한다.
                {
                    int weekday = Convert.ToInt32(reqTime.DayOfWeek);
                    // 요청받은 요일의 정규과목 목록을 가져옴
                    var regulars = repo.TimeTable.Where(e =>
                            e.Year == reqTime.Year && e.Term == term.Value &&
                            e.Weekday == weekday);

                    TimeTable timetable = null;
                    foreach(TimeTable t in regulars)
                    {
                        int[] periods = Util.SplitToInt(t.Period, ',');
                        int cnt = periods.Count(e => e.Equals(period));
                        if(cnt != 0)    // 요청된 교시에 수업이 존재한다.
                        {
                            timetable = t;
                            break;
                        }
                    }

                    // 찾은 수업 정보를 가지고 수강생 정보를 찾는다.
                    if(timetable != null)
                    {
                        // timetable로부터 course정보를 가져온다.
                        Course c = repo.Course.Find(timetable.Course);
                        if(c != null)   // course정보를 찾았다.
                        {
                            // 수강정보 및 수강자 정보를 찾았다.
                            var rc = repo.RegistCourse.Where(e =>
                                e.Year == reqTime.Year && e.Term == term &&
                                e.Course == c.Code);
                            // 수강생들 중 요청자와 일치하는 Number가 있다.
                            if (rc.SingleOrDefault(e => e.Number == member.Number) != null)
                                isMatch = true;
                        }
                    }
                }

                // 만약 isMatch가 true라면 이미 문을 열 수 있는 자격이 있음을 확인했으므로(정규수업시간임이 확인되었음으로)
                // 시설물 이용 신청 정보에서는 찾지 않도록 한다.
                if (!isMatch)
                {
                    // 요청받은 날짜의 시설물 이용정보를 가져옴
                    // 신청서 중 허가된 신청서의 데이터를 가져옴.
                    var usetable = repo.UseApplication.Where(e =>
                        e.Class == f.Id &&
                        e.Approval != null && e.Approval.Value &&
                        e.UseDate.Year == reqTime.Year &&
                        e.UseDate.Month == reqTime.Month &&
                        e.UseDate.Day == reqTime.Day);

                    // 이용시간이 겹치는 시설물을 찾아냄
                    foreach (var u in usetable)
                    {
                        int[] periods = Util.SplitToInt(u.Period, ',');
                        int cnt = periods.Count(e => e.Equals(period));
                        if (cnt != 0)    // 시설물 이용 정보를 찾아냄
                        {
                            if (u.Number == member.Number) // 대표신청자와 nfc태그정보가 일치함
                            {
                                isMatch = true;
                                break;
                            }
                        }
                    }
                }

                if (isMatch == false)
                    return false;

                // 시설물 상태를 열림으로 설정함
                f.IsLock = false;

                // 새로운 로그 기록
                var log = new UseLog()
                {
                    Facility = f.Id,
                    UnlockUser = member.Number,
                    UnlockDate = DateTime.Now,
                };
                repo.UseLog.Add(log);

                // 트랜젝션 반영
                repo.Entry(f).State = EntityState.Modified;
                repo.SaveChanges();
            }
            catch(NullReferenceException e)
            {
                return false;
            }
            return true;
        }

        public bool Lock(string nfc, string device, DateTime reqTime)
        {
            // 관리자이거나, 열었던 사람만 잠글 수 있음.
            try
            {
                var facility = repo.Facility.SingleOrDefault(e => e.Device == device);
                var card = repo.Nfclist.SingleOrDefault(e => e.Nfc == nfc);
                var member = repo.Member.Find(card.Number);

                // 로그기록으로부터 현재 시설물에 대한 마지막 로그 기록을 가져옴
                var log = repo.UseLog.Where(e => e.Facility == facility.Id).OrderByDescending(e => e.Id).Take(1);
                var l = log.ToArray()[0];
                // 잠김 기록이 있거나 열었던 사람과 다른 사람이거나 관리자가 아님.
                if (!(String.IsNullOrEmpty(l.LockUser) &&
                        (l.UnlockUser == member.Number || (member.Type == 3 && facility.Manager == member.Number))))
                    return false;

                // 로그에 닫은 기록 기입
                l.LockDate = DateTime.Now;
                l.LockUser = member.Number;

                // 시설물 상태를 잠김으로 함
                facility.IsLock = true;

                // 트렌젝션 반영
                repo.Entry(l).State = EntityState.Modified;
                repo.Entry(facility).State = EntityState.Modified;
                repo.SaveChanges();
                return true;
            }
            catch(NullReferenceException e)
            {
                return false;
            }
        }
    }
}
