﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class MemberProc : IMemberProcessor
    {
        public enum ErrorType
        {
            None = 0,
            NotFound = 1,
            BadData = 2,
        }

        private ErrorType error = ErrorType.None;
        public ErrorType error_code
        {
            get { return error; }
            set { error = value; }
        }

        private Repository repo = null;

        public MemberProc()
        {
            repo = new Repository();
        }

        public MemberProc(Repository entities)
        {
            repo = entities;
        }

        public IQueryable<Member> GetMembers()
        {
            return repo.Member;
        }

        public IQueryable<Member> GetMembers(int type)
        {
            return repo.Member.Where(e => e.Type == type);
        }

        public Member GetMember(string number)
        {
            return repo.Member.SingleOrDefault(member => member.Number == number);
        }

        public bool AddMember(Member data)
        {
            try
            {
                // password sha1 encoding
                data.Password = Util.PasswordCrypt(data.Password);
                repo.Member.Add(data);
                repo.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool UpdateMember(string number, Member data)
        {
            if (!number.Equals(data.Number))
                return false;   // 데이터 이상

            repo.Entry(data).State = EntityState.Modified;
            try
            {
                repo.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (repo.Member.Count(member => member.Number == number) <= 0)
                    error_code = ErrorType.NotFound;    // 데이터가 없음
                else
                    error_code = ErrorType.BadData;     // 잘못된 데이터
                
                return false;
            }

            return true;
        }

        public Member DeleteMember(string number)
        {
            Member member = repo.Member.Find(number);   // key값인 학번/직원번호(number)를 이용하여 데이터를 찾음
            if (member == null)                             // 그런거 없음
            {
                error_code = ErrorType.NotFound;
                return null;
            }

            repo.Member.Remove(member);                 // 삭제
            repo.SaveChanges();                         // 저장

            return member;
        }

        public Member FindToken(string token)
        {
            return repo.Member.SingleOrDefault(e => e.Token == token);
        }

        public bool AttachGcmId(Member member, string regid)
        {
            member.GcmId = regid;
            repo.Entry(member).State = EntityState.Modified;
            repo.SaveChanges();
            return true;
        }

        public bool DetachGcmId(Member member)
        {
            member.GcmId = "";
            repo.Entry(member).State = EntityState.Modified;
            repo.SaveChanges();
            return true;
        }

        public string GetGcmId(string number)
        {
            Member member = repo.Member.Find(number);
            if (member == null)
            {
                error_code = ErrorType.NotFound;
                throw new NullReferenceException();
            }
            return member.GcmId;
        }

        public bool AttachToken(Member member, string token)
        {
            member.Token = token;
            repo.Entry(member).State = EntityState.Modified;
            repo.SaveChanges();

            return true;
        }

        public bool DetachToken(Member member)
        {
            member.Token = "";
            repo.Entry(member).State = EntityState.Modified;
            repo.SaveChanges();

            return true;
        }
    }
}
