﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class FacilityProc : IFacilityProcessor
    {
        private Repository repo;

        public FacilityProc()
        {
            repo = new Repository();
        }

        public FacilityProc(Repository entities)
        {
            repo = entities;
        }

        public IQueryable<Facility> GetFacilities()
        {
            return repo.Facility;
        }

        public IQueryable<Facility> GetFacilities(Building building)
        {
            return repo.Facility.Where(e => e.Building == building.Id);
        }

        public IQueryable<Facility> GetFacilities(Building building, int floar)
        {
            return repo.Facility.Where(e => e.Building == building.Id && e.Floor == floar);
        }

        public Facility GetFacility(int idx)
        {
            return repo.Facility.Find(idx);
        }

        public Facility GetFacility(string device)
        {
            return repo.Facility.SingleOrDefault(e => e.Device == device);
        }

        public ICollection<int> GetSchedules(int idx, DateTime now)
        {
            ICollection<int> schedules = new Collection<int>();

            int year = now.Year;
            int? term = Util.MonthToTerm(now.Month);
            int weekday = (int)now.DayOfWeek;

            // 학기중이 아니라면 정규수업 테이블은 의미가 없으므로 null을 리턴.
            if (term != null)
            {
                // 해당 요일의 해당 시설물의 해당 학기의 해당 해의 모든 정규수업을 가져옴.
                // 좀 더 깔끔한 방법이 없을까? -_-a
                var tresult =
                    repo.TimeTable.Where(e => e.Weekday == weekday &&
                                              e.Class == idx &&
                                              e.Year == year &&
                                              e.Term == term.Value);

                foreach (var timetable in tresult)
                {
                    var periods = Util.SplitToInt(timetable.Period, ',');
                    foreach (int n in periods)
                        schedules.Add(n);
                }
            }

            // 신청 정보에서 가져옴.
            var aresult = repo.UseApplication.Where(e => e.UseDate.Year == now.Year &&
                                                         e.UseDate.Month == now.Month &&
                                                         e.UseDate.Day == now.Day &&
                                                         e.Class == idx)
                .Where(e => (e.Approval == null || e.Approval.Value));
            // Null(응답하지 않음)이거나 False가 아닌 경우(리젝되지 않음) 포함됨.
            // 두개의 승인 중 하나라도 리젝된다면 그 요청은 사용할 수 없음.

            foreach (var application in aresult)
                foreach (int n in Util.SplitToInt(application.Period, ','))
                    schedules.Add(n);

            return schedules;
        }

        public bool Create(Facility c)
        {
            repo.Facility.Add(c);
            repo.SaveChanges();
            return true;
        }

        public bool Update(int id, Facility c)
        {
            if (id != c.Id)
                return false;

            repo.Entry(c).State = EntityState.Modified;
            try
            {
                repo.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (repo.Facility.Count(e => e.Id == id) > 0) // id값이 잘못됨
                    return false;   // 실패 리턴
                else
                    throw ex;       // 예외 쓰로!
            }
            
            return true;
        }

        public bool Delete(int id)
        {
            var @class = repo.Facility.Find(id);
            if (@class == null)
                return false;

            repo.Facility.Remove(@class);
            repo.SaveChanges();
            return true;
        }

        public string ToString(Facility c)
        {
            Building b = repo.Building.Find(c.Building);
            return b.Name + " " + Convert.ToString(c.Floor) + Convert.ToString(c.Name) + "호";
        }
    }
}
