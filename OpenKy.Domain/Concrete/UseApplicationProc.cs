﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Proxies;
using System.Text;
using System.Threading.Tasks;
using Htna.Gcm;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class UseApplicationProc : IUseApplicationProcessor
    {
        private Repository repo;

        public UseApplicationProc()
        {
            repo = new Repository();
        }

        public UseApplicationProc(Repository entities)
        {
            repo = entities;
        }

        public bool Request(UseApplication rq)
        {
            int? term = Util.MonthToTerm(rq.UseDate.Month);
            if (term != null)   // 정규 시간 테이블과 비교한다.
            {
                ITimetableProcessor timetable = new TimeTableProc(repo);
                if (timetable.IsOverlap(rq.Class, rq.UseDate.Year, (int) term, (int) rq.UseDate.DayOfWeek, rq.Period))
                    return false;
            }

            // 사용 예약 테이블과 비교한다.
            // 등록하고자 하는 날짜, 시간에 거부되지 않은 모든 신청서를 대상으로 함.
            var applications = repo.UseApplication.Where(e => e.Class == rq.Class &&                    // 시설물
                                                              e.UseDate.Year == rq.UseDate.Year &&      // 년도
                                                              e.UseDate.Month == rq.UseDate.Month &&    // 월
                                                              e.UseDate.Day == rq.UseDate.Day &&        // 일
                                                              (e.Approval == null || e.Approval.Value));  // 가불되지 않았거나, 허가된
            // 신청서의 교시정보를 가져와서 겹치는지 비교한다.
            var table = new int[15];
            foreach(var t in applications)
            {
                int[] p = Util.SplitToInt(t.Period, ',');   // 해당 교시에 사용예약이 있으면 1을 더한다.
                foreach (int n in p)
                    table[n] += 1;
            }
            int[] k = Util.SplitToInt(rq.Period, ',');
            foreach (var n in k)
                table[n] += 1;

            // 1보다 큰 값이 있다는 건 겹친다는 뜻이다.
            if (table.Where(e => e > 1).Any())
                return false;

            // 데이터베이스에 추가한다.
            repo.UseApplication.Add(rq);
            repo.SaveChanges();

            return true;
        }

        public bool Cancel(int id)
        {
            var result = repo.UseApplication.Single(e => e.Id == id);
            if (result == null)
                return false;

            repo.UseApplication.Remove(result);
            repo.SaveChanges();
            return true;
        }

        public bool Accept(int id, string number)
        {
            try
            {
                // 신청서를 가져온다.
                UseApplication application = repo.UseApplication.Find(id);

                // 신청서에 대한 허가권한이 있는지 검사한다.
                // 먼저 유저 데이터를 가져옴
                Member user = repo.Member.Find(number);
                if (user.Type != 3)
                    return false;       // 교직원이 아닌 사람에게 허가, 거부 권한은 없음.

                Facility facility = repo.Facility.Find(application.Class);  // 사용하고자 하는 시설물 정보 가져옴
                if (facility.Manager != number) // 관리자가 아님
                    return false;

                application.Approval = true;

                repo.Entry(application).State = EntityState.Modified;
                repo.SaveChanges();

                return true;
            }
            catch (NullReferenceException e)
            {
                return false;
            }
        }

        public bool Reject(int id, string number)
        {
            try
            {
                // 신청서를 가져온다.
                UseApplication application = repo.UseApplication.Find(id);

                // 신청서에 대한 허가권한이 있는지 검사한다.
                // 먼저 유저 데이터를 가져옴
                Member user = repo.Member.Find(number);
                if (user.Type != 3)
                    return false;       // 교직원이 아닌 사람에게 허가, 거부 권한은 없음.

                Facility facility = repo.Facility.Find(application.Class);  // 사용하고자 하는 시설물 정보 가져옴
                if (facility.Manager != number) // 관리자가 아님
                    return false;
                 
                application.Approval = false;
                repo.Entry(application).State = EntityState.Modified;
                repo.SaveChanges();

                return true;
            }
            catch (NullReferenceException e)
            {
                return false;
            }
        }

        public IQueryable<UseApplication> GetRequests()
        {
            return repo.UseApplication;
        }

        public IQueryable<UseApplication> GetRequests(string number)
        {
            return repo.UseApplication.Where(e => e.Number == number);
        }

        public UseApplication GetRequest(int id)
        {
            return repo.UseApplication.Find(id);
        }

        public IQueryable<UseApplication> GetRequestForClass(int @class)
        {
            return repo.UseApplication.Where(e => e.Class == @class);
        }

        public IQueryable<UseApplication> GetRequestForManager(string number)
        {
            // number가 관리하는 건물 목록을 가져옴
            IQueryable<Facility> facilities = repo.Facility.Where(e => e.Manager == number);
            return facilities.SelectMany(f => repo.UseApplication.Where(e => e.Class == f.Id && e.Approval == null)).OrderBy(e=>e.ApplicationDate);
        }

        public IQueryable<UseApplication> GetRequestForProfessor(string number)
        {
            // 교수가 지도교수인 학생들의 목록을 가져옴
            // 각 학생들이 신청자이면서 현재시간보다 미래시간에 사용이 예정된 정보를 가져옴
            var students = repo.Student.Where(e => e.Professor == number);
            return
                students.SelectMany(
                    f => repo.UseApplication.Where(e => e.Number == f.Number && e.UseDate > DateTime.Now));
        }
    }
}
