﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class StudentProc : IStudentProcessor
    {
        Repository repo = new Repository();

        public IQueryable<Student> GetStudents()
        {
            return repo.Student;
        }

        public Student GetStudent(string number)
        {
            return repo.Student.Find(number);
        }

        public bool AddStudent(Student student)
        {
            repo.Student.Add(student);
            repo.SaveChanges();
            return true;
        }

        public bool UpdateStudent(string number, Student student)
        {
            if (number != student.Number)
                return false;

            repo.Entry(student).State = EntityState.Modified;
            repo.SaveChanges();
            return true;
        }

        public bool DeleteStudent(string number)
        {
            var info = repo.Student.Find(number);
            if(info != null)
            {
                repo.Student.Remove(info);
                repo.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
