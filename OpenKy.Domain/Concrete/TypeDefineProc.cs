﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class TypeDefineProc : ITypeDefineProcessor
    {
        private Repository repo;

        public TypeDefineProc()
        {
            repo = new Repository();
        }

        public TypeDefineProc(Repository repository)
        {
            repo = repository;
        }

        public IQueryable<TypeDefine> GetTypeDefs()
        {
            return repo.TypeDefine;
        }

        public TypeDefine GetTypeDef(int id)
        {
            return repo.TypeDefine.Find(id);
        }

        public TypeDefine FindType(string typename)
        {
            return repo.TypeDefine.Single(e => e.Name == typename);
        }

        public bool CreateType(TypeDefine t)
        {
            repo.TypeDefine.Add(t);
            repo.SaveChanges();

            return true;
        }

        public bool UpdateType(int type, TypeDefine t)
        {
            if (type != t.Type)
                return false;

            repo.Entry(t).State = EntityState.Modified;
            repo.SaveChanges();
            return true;
        }

        public bool DeleteType(int type)
        {
            TypeDefine t = repo.TypeDefine.Find(type);
            if (t == null)
                return false;

            repo.TypeDefine.Remove(t);
            repo.SaveChanges();
            return true;
        }
    }
}
