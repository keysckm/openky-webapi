﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Htna.Gcm;
using OpenKy.Domain.Abstract;

namespace OpenKy.Domain.Concrete
{
    public class GcmNotification : IGcmNotification
    {
        private Gcm gcm;

        public GcmNotification()
        {
            GcmInit("864844122078", "AIzaSyBwT-QUxwwMNciUOdx3rPwgcftBoP6X9uA");
        }

        private void GcmInit(string project_number, string server_api_key)
        {
            // GCM 초기화
            gcm = new Gcm
            {
                ProjectNumber = project_number, 
                ServerApiKey = server_api_key
            };
        }

        public bool Push(string number, Dictionary<string, string> msgs)
        {
            // TODO: 이곳에 push알림을 송신하는 로직을 삽입합니다.
            // Member테이블로부터 DeviceId(regid)를 가져와서 송신합니다.
            try
            {
                IMemberProcessor members = new MemberProc();
                string regid = members.GetGcmId(number);
                string response = gcm.SendNotification(regid, msgs);
            }
            catch (NullReferenceException e)
            {
                return false;
            }
            return true;
        }

        public bool Push(string number, string msg)
        {
            // TODO: 이곳에 push알림을 송신하는 로직을 삽입합니다.
            // Member테이블로부터 DeviceId(regid)를 가져와서 송신합니다.
            try
            {
                IMemberProcessor members = new MemberProc();
                string regid = members.GetGcmId(number);
                string response = gcm.SendNotification(regid, msg);
            }
            catch (NullReferenceException e)
            {
                return false;
            }
            return true;
        }
    }
}
