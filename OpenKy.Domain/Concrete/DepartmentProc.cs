﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class DepartmentProc : IDepartmentProcessor
    {
        private Repository repo;

        public DepartmentProc()
        {
            repo = new Repository();
        }

        public DepartmentProc(Repository r)
        {
            repo = r;
        }

        public IQueryable<Department> GetDepartments()
        {
            return repo.Department;
        }

        public Department GetDepartment(string code)
        {
            return repo.Department.Find(code);
        }
    }
}
