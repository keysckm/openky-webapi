﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class LogProc : ILogProcessor
    {
        Repository repo = new Repository();

        public IQueryable<UseLog> GetLogs(int fid, DateTime date)
        {
            return repo.UseLog.Where(e => e.Facility == fid &&
                                          e.UnlockDate.Year == date.Year && 
                                          e.UnlockDate.Month == date.Month &&
                                          e.UnlockDate.Day == date.Day &&
                                          e.LockDate != null);
        }
    }
}
