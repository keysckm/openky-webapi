﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class CourseProc : ICourseProcessor
    {
        private Repository repo;
        public CourseProc()
        {
            repo = new Repository();
        }

        public CourseProc(Repository repo)
        {
            this.repo = repo;
        }

        public IQueryable<Course> GetCourses()
        {
            return repo.Course;
        }

        public Course GetCourse(string id)
        {
            return repo.Course.Find(id);
        }

        public bool AddCourse(Course course)
        {
            repo.Course.Add(course);
            repo.SaveChanges();
            
            return true;
        }

        public bool UpdateCourse(string id, Course course)
        {
            if (id != course.Code)
                return false;

            repo.Entry(course).State = EntityState.Modified;
            repo.SaveChanges();
            return true;
        }

        public bool DeleteCourse(string id)
        {
            Course result = repo.Course.Find(id);
            if (result == null)
                return false;

            repo.Course.Remove(result);
            repo.SaveChanges();
            return true;
        }
    }
}
