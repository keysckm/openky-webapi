﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;

namespace OpenKy.Domain.Concrete
{
    public class TimeTableProc : ITimetableProcessor
    {
        private Repository repo;

        public TimeTableProc(Repository entities)
        {
            repo = entities;
        }

        public IQueryable<TimeTable> GetTimeTables()
        {
            return repo.TimeTable;
        }

        public IQueryable<TimeTable> GetTimeTables(int classid, int weekday)
        {
            return repo.TimeTable.Where(e => e.Weekday == weekday && e.Class == classid);
        }

        public bool IsOverlap(int @class, int year, int term, int weekday, string period)
        {
            // 년도, 학기, 요일이 일치하는 시간표 정보를 가져온다.
            var result = repo.TimeTable.Where(e => e.Class == @class && e.Year == year && e.Term == term && e.Weekday == weekday);
            int[] table = new int[15];
            foreach(TimeTable t in result)          //
            {                                       // 15개짜리 int배열을 요일 시간표 테이블로 하여
                int[] p = Util.SplitToInt(t.Period, ',');   // 해당 교시에 사용예약이 있으면 1을 더한다.
                foreach(int n in p)
                    table[n] += 1;
            }

            int[] k = Util.SplitToInt(period, ',');         // 마찬가지
            foreach(int n in k)
                table[n] += 1;

            return table.Count(e => e > 1) > 0;
        }

        public TimeTable AddTimeTable(TimeTable data)
        {
            // 데이터를 지정된 곳에 삽입할 수 있는지 검사
            if (IsOverlap(data.Class, data.Year, data.Term, data.Weekday, data.Period))
                return null;

            repo.TimeTable.Add(data);
            repo.SaveChanges();

            return data;
        }

        public TimeTable UpdateTimeTable(int id, TimeTable data)
        {
            if (id != data.Id)
                return null;

            // data에 지정된 년, 학기, 요일의 데이터를 가져온다.
            // 단, data와 같은 id를 가진 데이터는 제외한다.
            var result =
                repo.TimeTable.Where(
                    e => e.Id != data.Id && e.Year == data.Year && e.Term == data.Term && e.Weekday == data.Weekday);
            
            int[] table = new int[15];
            foreach(TimeTable t in result)
            {
                int[] arr = Util.SplitToInt(t.Period, ',');
                foreach(int n in arr)
                    table[n] += 1;
            }

            foreach (int n in Util.SplitToInt(data.Period, ','))
                table[n] += 1;

            // table에 1보다 큰 값이 있다면 겹친다는 뜻이다.
            if (table.Where(e => e > 1).Any())
                return null;

            repo.Entry(data).State = EntityState.Modified;
            repo.SaveChanges();

            return data;
        }

        public TimeTable DeleteTimeTable(int id)
        {
            TimeTable t = repo.TimeTable.Single(e => e.Id == id);
            if (t == null)
                return null;

            repo.TimeTable.Remove(t);
            repo.SaveChanges();

            return t;
        }
    }
}
