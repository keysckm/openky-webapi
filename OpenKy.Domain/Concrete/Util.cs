﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OpenKy.Domain.Concrete
{
    public class Util
    {
        public static string PasswordCrypt(string src, string salt = "openkysalt!@#$%^&*()")
        {
            string key = src + salt;
            byte[] result;
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            result = sha1.ComputeHash(Encoding.UTF8.GetBytes(key));
            StringBuilder sb = new StringBuilder();
            foreach(string part in BitConverter.ToString(result).Split('-'))
            {
                sb.Append(part);
            }

            return sb.ToString().ToLower();
        }

        /// <summary>
        /// 특정 시각을 교시로 바꿉니다.
        /// </summary>
        /// <param name="hour">교시로 바꿀 시각입니다.</param>
        /// <returns>int를 리턴합니다. 정규 수업 범위를 벗어나면 0을 리턴합니다.</returns>
        public static int TimeToPeriod(int hour)
        {
            if (hour < 9 || hour > 24)
                return 0;

            return hour - 8;
        }

        /// <summary>
        /// split으로 구분된 문자열을 숫자로 변환하여 배열로 리턴합니다.
        /// 예를 들 문자열 "2, 3, 4, 5, 6"은 int형 배열 2, 3, 4, 5, 6으로 변환됩니다.
        /// </summary>
        /// <param name="str">변환 될 숫자가 있는 문자열입니다.</param>
        /// <param name="split">숫자를 구분할 토큰입니다.</param>
        /// <returns>int[]를 리턴합니다. 만약 숫자가 아닌 문자열이 있다면 null을 리턴합니다.</returns>
        public static int[] SplitToInt(string str, char split)
        {
            var strs = str.Split(split);
            int[] results = new int[strs.Count()];

            try
            {
                for (int i = 0; i < strs.Count(); i++)
                    results[i] = Convert.ToInt32(strs[i]);
            }
            catch(FormatException e)
            {
                return null;
            }

            return results;
        }

        /// <summary>
        /// 월을 학기로 변환합니다.
        /// 학기중이 아니라면 null을 반환합니다.
        /// </summary>
        /// <param name="month">월 입니다.</param>
        /// <returns>int?를 반환합니다.</returns>
        public static int? MonthToTerm(int month)
        {
            switch(month)
            {
                case 3:
                case 4:
                case 5:
                case 6:
                    return 1;
                case 9:
                case 10:
                case 11:
                case 12:
                    return 2;
                default:
                    return null;
            }
        }
    }
}
