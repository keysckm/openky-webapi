﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Concrete;
using OpenKy.Domain.Entities;
using OpenKy.WebUI.Concrete;

namespace OpenKy.WebUI.Controllers
{
    [Auth(Roles = UserType.Staff | UserType.Professor)]
    public class TypeDefinesController : Controller
    {
        private Repository db = new Repository();
        private ITypeDefineProcessor typedef_proc;

        public TypeDefinesController()
        {
            typedef_proc = new TypeDefineProc(db);
        }

        // GET: TypeDefines
        public ActionResult Index()
        {
            return View(typedef_proc.GetTypeDefs().ToList());
        }

        // GET: TypeDefines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeDefine typeDefine = typedef_proc.GetTypeDef(id.Value);
            if (typeDefine == null)
            {
                return HttpNotFound();
            }
            return View(typeDefine);
        }

        // GET: TypeDefines/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TypeDefines/Create
        // 초과 게시 공격으로부터 보호하려면 바인딩하려는 특정 속성을 사용하도록 설정하십시오. 
        // 자세한 내용은 http://go.microsoft.com/fwlink/?LinkId=317598을(를) 참조하십시오.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Type,Name")] TypeDefine typeDefine)
        {
            if (ModelState.IsValid)
            {
                if(!typedef_proc.UpdateType(typeDefine.Type, typeDefine))
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                return RedirectToAction("Index");
            }

            return View(typeDefine);
        }

        // GET: TypeDefines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeDefine typeDefine = typedef_proc.GetTypeDef(id.Value);
            if (typeDefine == null)
            {
                return HttpNotFound();
            }
            return View(typeDefine);
        }

        // POST: TypeDefines/Edit/5
        // 초과 게시 공격으로부터 보호하려면 바인딩하려는 특정 속성을 사용하도록 설정하십시오. 
        // 자세한 내용은 http://go.microsoft.com/fwlink/?LinkId=317598을(를) 참조하십시오.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Type,Name")] TypeDefine typeDefine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeDefine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(typeDefine);
        }

        // GET: TypeDefines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeDefine typeDefine = typedef_proc.GetTypeDef(id.Value);
            if (typeDefine == null)
            {
                return HttpNotFound();
            }
            return View(typeDefine);
        }

        // POST: TypeDefines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!typedef_proc.DeleteType(id))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
