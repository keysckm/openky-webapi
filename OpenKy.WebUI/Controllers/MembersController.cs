﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Concrete;
using OpenKy.Domain.Entities;
using OpenKy.WebUI.Concrete;
using OpenKy.WebUI.Model;

namespace OpenKy.WebUI.Controllers
{
    [Auth(Roles = UserType.Staff | UserType.Professor)]
    public class MembersController : Controller
    {
        private Repository db = new Repository();
        private IMemberProcessor member_proc;
        private ITypeDefineProcessor typedef_proc;
        private IDepartmentProcessor department_proc;

        public MembersController()
        {
            member_proc = new MemberProc(db);
            typedef_proc = new TypeDefineProc(db);
            department_proc = new DepartmentProc(db);
        }

        // GET: Members
        public ActionResult Index()
        {
            ViewBag.TypeDefines = typedef_proc.GetTypeDefs();
            ViewBag.Departments = department_proc.GetDepartments();
            return View(member_proc.GetMembers().ToList());
        }

        // GET: Members/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = member_proc.GetMember(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        void PushDropdownListItem()
        {
            var lstTypes = typedef_proc.GetTypeDefs().Select(type => new SelectListItem()
            {
                Text = type.Name, Value = type.Type.ToString()
            }).ToList();

            var lstDepartments = department_proc.GetDepartments().Select(department => new SelectListItem()
            {
                Text = "[" + department.Code + "] " + department.Name, Value = department.Code
            }).ToList();

            ViewBag.TypeList = lstTypes;
            ViewBag.DepartmentList = lstDepartments;
        }

        // GET: Members/Create
        public ActionResult Create()
        {
            PushDropdownListItem();
            return View();
        }

        // POST: Members/Create
        // 초과 게시 공격으로부터 보호하려면 바인딩하려는 특정 속성을 사용하도록 설정하십시오. 
        // 자세한 내용은 http://go.microsoft.com/fwlink/?LinkId=317598을(를) 참조하십시오.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Number,Password,Name,Phone,Email,Birthday,Department,Type")] Member member)
        {
            if (ModelState.IsValid)
            {
                if(member_proc.AddMember(member))
                    return RedirectToAction("Index");
            }

            return View(member);
        }

        // GET: Members/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = member_proc.GetMember(id);
            if (member == null)
            {
                return HttpNotFound();
            }

            MemberModel viewmodel = new MemberModel()
            {
                Birthday = member.Birthday,
                Department = member.Department,
                Email = member.Email,
                Name = member.Name,
                Number = member.Number,
                Password = null,
                Phone = member.Phone,
                Type = member.Type
            };

            PushDropdownListItem();

            return View(viewmodel);
        }

        // POST: Members/Edit/5
        // 초과 게시 공격으로부터 보호하려면 바인딩하려는 특정 속성을 사용하도록 설정하십시오. 
        // 자세한 내용은 http://go.microsoft.com/fwlink/?LinkId=317598을(를) 참조하십시오.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Number,Password,Name,Phone,Email,Birthday,Department,Type")] MemberModel member)
        {
            if (ModelState.IsValid)
            {
                Member original = member_proc.GetMember(member.Number);
                if (original == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                if (!string.IsNullOrEmpty(member.Password))
                {
                    string passwd = Util.PasswordCrypt(member.Password);
                    original.Password = passwd;
                }

                original.Name = member.Name;
                original.Phone = member.Phone;
                original.Email = member.Email;
                original.Birthday = member.Birthday;
                original.Department = member.Department;
                original.Type = member.Type;

                if (!member_proc.UpdateMember(member.Number, original))
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                return RedirectToAction("Index");
            }
            return View(member);
        }

        // GET: Members/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = member_proc.GetMember(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        // POST: Members/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if (member_proc.DeleteMember(id) == null)
                return HttpNotFound();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
