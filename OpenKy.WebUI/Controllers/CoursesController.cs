﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Concrete;
using OpenKy.Domain.Entities;
using OpenKy.WebUI.Concrete;

namespace OpenKy.WebUI.Controllers
{
    [Auth(Roles = UserType.Staff | UserType.Professor)]
    public class CoursesController : Controller
    {
        private Repository db = new Repository();
        private ICourseProcessor course_proc;
        private IMemberProcessor member_proc;
        private ITypeDefineProcessor typedef_proc;
        private IDepartmentProcessor depart_proc;

        public CoursesController()
        {
            course_proc = new CourseProc(db);
            member_proc = new MemberProc(db);
            typedef_proc = new TypeDefineProc(db);
            depart_proc = new DepartmentProc(db);
        }

        // GET: Courses
        public ActionResult Index()
        {
            TypeDefine professor_type = typedef_proc.FindType("교수");
            var professors = member_proc.GetMembers(professor_type.Type);

            ViewBag.ProfessorList = professors;
            return View(course_proc.GetCourses());
        }

        // GET: Courses/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = course_proc.GetCourse(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        void PushDropdownListItem()
        {
            var lstProfessor = new List<SelectListItem>();
            TypeDefine professor_type = typedef_proc.FindType("교수");

            var professors = member_proc.GetMembers(professor_type.Type);
            if (professors.Any())
            {
                lstProfessor.AddRange(professors.Select(m => new SelectListItem()
                {
                    Text = m.Name + " (" + depart_proc.GetDepartment(m.Department).Name + ")", Value = m.Number
                }));
            }

            ViewBag.ProfessorList = lstProfessor;
        }

        // GET: Courses/Create
        public ActionResult Create()
        {
            PushDropdownListItem();
            return View();
        }

        // POST: Courses/Create
        // 초과 게시 공격으로부터 보호하려면 바인딩하려는 특정 속성을 사용하도록 설정하십시오. 
        // 자세한 내용은 http://go.microsoft.com/fwlink/?LinkId=317598을(를) 참조하십시오.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Code,Name,Divide,Professor,Credits,Theory,Training")] Course course)
        {
            if (ModelState.IsValid)
            {
                if (!course_proc.AddCourse(course))
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                return RedirectToAction("Index");
            }

            return View(course);
        }

        // GET: Courses/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = course_proc.GetCourse(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            PushDropdownListItem();
            return View(course);
        }

        // POST: Courses/Edit/5
        // 초과 게시 공격으로부터 보호하려면 바인딩하려는 특정 속성을 사용하도록 설정하십시오. 
        // 자세한 내용은 http://go.microsoft.com/fwlink/?LinkId=317598을(를) 참조하십시오.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Code,Name,Divide,Professor,Credits,Theory,Training")] Course course)
        {
            if (ModelState.IsValid)
            {
                course_proc.UpdateCourse(course.Code, course);
                return RedirectToAction("Index");
            }
            return View(course);
        }

        // GET: Courses/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = course_proc.GetCourse(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if(!course_proc.DeleteCourse(id))
                return new HttpNotFoundResult();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
