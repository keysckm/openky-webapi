﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Concrete;

namespace OpenKy.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private Repository repo = new Repository();

        // GET: Home
        public ActionResult Index()
        {
            ViewBag.building_count = repo.Building.Count();
            ViewBag.course_count = repo.Course.Count();
            ViewBag.facility_count = repo.Facility.Count();
            ViewBag.member_count = repo.Member.Count();

            return View();
        }
    }
}