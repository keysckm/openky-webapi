﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OpenKy.Domain.Concrete;
using OpenKy.WebUI.Concrete;
using OpenKy.WebUI.Model;

namespace OpenKy.WebUI.Controllers
{
    public class AccountController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginModel model)
        {
            if(!ModelState.IsValid)
            {
                return View(model);
            }

            string pwd = Util.PasswordCrypt(model.Password);
            Repository repo = new Repository();
            var member = repo.Member.Single(e => e.Number == model.Number && e.Password == pwd);
            if(member == null)
            {
                ModelState.AddModelError("", "아이디 혹은 비밀번호가 일치하지 않습니다.");
                return View(model);
            }

            Session["Number"] = member.Number;
            UserType type;
            switch(member.Type)
            {
                case 1:
                    type = UserType.Student;
                    break;

                case 2:
                    type = UserType.Professor;
                    break;

                case 3:
                    type = UserType.Staff;
                    break;

                default:
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
            Session["UserType"] = type;
            Session["Name"] = member.Name;

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            if (Session["Name"] == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Session["Number"] = null;
            Session["UserType"] = null;
            Session["Name"] = null;

            return RedirectToAction("Index", "Home");
        }
    }
}