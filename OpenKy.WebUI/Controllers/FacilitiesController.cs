﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Concrete;
using OpenKy.Domain.Entities;
using OpenKy.WebUI.Concrete;

namespace OpenKy.WebUI.Controllers
{
    [Auth(Roles = UserType.Staff | UserType.Professor)]
    public class FacilitiesController : Controller
    {
        private Repository db = new Repository();
        private IBuildingProcessor building_proc;
        private IMemberProcessor member_proc;
        private ITypeDefineProcessor typedef_proc;

        public FacilitiesController()
        {
            building_proc = new BuildingProc(db);
            member_proc = new MemberProc(db);
            typedef_proc = new TypeDefineProc(db);
        }

        // GET: Facilities
        public ActionResult Index()
        {
            ViewBag.BuildingList = building_proc.GetBuildings();
            ViewBag.MemberList = member_proc.GetMembers();
            return View(db.Facility.ToList());
        }

        // GET: Facilities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facility facility = db.Facility.Find(id);
            if (facility == null)
            {
                return HttpNotFound();
            }
            return View(facility);
        }
        
        void PushDropdownListItem()
        {
            var lstBuildings = new List<SelectListItem>();
            TypeDefine staff = typedef_proc.FindType("교직원");

            var lstManagers = member_proc.GetMembers().Where(e => e.Type == staff.Type).Select(m => new SelectListItem()
            {
                Text = m.Name + " [" + m.Number + "]", Value = m.Number
            }).ToList();

            lstBuildings.Add(new SelectListItem()
            {
                Text = "야외시설물",
                Value = "0"
            });
            lstBuildings.AddRange(building_proc.GetBuildings().Select(b => new SelectListItem()
            {
                Text = b.Name, Value = b.Id.ToString()
            }));

            ViewBag.ManagerList = lstManagers;
            ViewBag.BuildingList = lstBuildings;
        }

        // GET: Facilities/Create
        public ActionResult Create()
        {
            PushDropdownListItem();
            return View();
        }

        // POST: Facilities/Create
        // 초과 게시 공격으로부터 보호하려면 바인딩하려는 특정 속성을 사용하도록 설정하십시오. 
        // 자세한 내용은 http://go.microsoft.com/fwlink/?LinkId=317598을(를) 참조하십시오.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Floor,Name,Building,Manager")] Facility facility)
        {
            if (ModelState.IsValid)
            {
                db.Facility.Add(facility);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(facility);
        }

        // GET: Facilities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facility facility = db.Facility.Find(id);
            if (facility == null)
            {
                return HttpNotFound();
            }

            PushDropdownListItem();
            return View(facility);
        }

        // POST: Facilities/Edit/5
        // 초과 게시 공격으로부터 보호하려면 바인딩하려는 특정 속성을 사용하도록 설정하십시오. 
        // 자세한 내용은 http://go.microsoft.com/fwlink/?LinkId=317598을(를) 참조하십시오.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Floor,Name,Building,Manager")] Facility facility)
        {
            if (ModelState.IsValid)
            {
                db.Entry(facility).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(facility);
        }

        // GET: Facilities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facility facility = db.Facility.Find(id);
            if (facility == null)
            {
                return HttpNotFound();
            }
            return View(facility);
        }

        // POST: Facilities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Facility facility = db.Facility.Find(id);
            db.Facility.Remove(facility);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
