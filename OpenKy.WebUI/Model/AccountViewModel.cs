﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OpenKy.WebUI.Model
{
    public class LoginModel
    {
        [Required]
        [StringLength(20)]
        public string Number { get; set; }

        [StringLength(100, MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}