namespace OpenKy.WebUI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Nfclist")]
    public partial class Nfclist
    {
        [StringLength(20)]
        public string Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Number { get; set; }
    }
}
