namespace OpenKy.WebUI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Uselog")]
    public partial class Uselog
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Number { get; set; }

        public int Class { get; set; }

        public bool Lock { get; set; }

        public DateTime UnlockDate { get; set; }

        public DateTime? LockDate { get; set; }
    }
}
