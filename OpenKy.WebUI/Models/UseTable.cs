namespace OpenKy.WebUI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UseTable")]
    public partial class UseTable
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Number { get; set; }

        public int Class { get; set; }

        [Column(TypeName = "date")]
        public DateTime Reserve { get; set; }

        [Required]
        [StringLength(50)]
        public string Period { get; set; }
    }
}
