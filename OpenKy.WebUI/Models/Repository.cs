namespace OpenKy.WebUI.Models
{
    using System.Data.Entity;

    public partial class Repository : DbContext
    {
        public Repository()
            : base("name=Repository")
        {
        }

        public virtual DbSet<Building> Building { get; set; }
        public virtual DbSet<Course> Course { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Facility> Facility { get; set; }
        public virtual DbSet<Member> Member { get; set; }
        public virtual DbSet<Nfclist> Nfclist { get; set; }
        public virtual DbSet<Picture> Picture { get; set; }
        public virtual DbSet<TimeTable> TimeTable { get; set; }
        public virtual DbSet<TypeDefine> TypeDefine { get; set; }
        public virtual DbSet<UseApplication> UseApplication { get; set; }
        public virtual DbSet<Uselog> Uselog { get; set; }
        public virtual DbSet<UseTable> UseTable { get; set; }
    }
}
