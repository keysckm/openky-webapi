namespace OpenKy.WebUI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TypeDefine")]
    public partial class TypeDefine
    {
        [Key]
        public int Type { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }
    }
}
