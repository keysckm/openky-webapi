﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OpenKy.WebUI.Concrete
{
    [Flags]
    public enum UserType
    {
        Student = 1,
        Professor = 2,
        Staff = 4,
    }

    public class Auth : AuthorizeAttribute
    {
        public new UserType Roles;
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if(httpContext == null)
            {
                throw new ArgumentNullException("httpContext is null");
            }

            if (httpContext.Session["UserType"] == null || httpContext.Session["UserType"].ToString() == "")
                return false;

            var role = (UserType) Convert.ToInt32(httpContext.Session["UserType"]);

            if (Roles != 0 && (Roles & role) != role)
                return false;

            return true;
        }
    }
}