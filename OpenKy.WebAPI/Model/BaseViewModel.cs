﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OpenKy.WebAPI.Model
{
    /// <summary>
    /// 베이스 뷰 모델
    /// 인증이 필요한 경우 Number와 Token을 비교해서 인증합니다.
    /// </summary>
    public class BaseViewModel
    {
        [Required]
        public string Number { get; set; }

        [Required]
        public string Token { get; set; }
    }
}