﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OpenKy.WebAPI.Model
{
    /// <summary>
    /// 클라이언트로부터 로그인에 사용하는 모델입니다.
    /// </summary>
    public class LoginViewModel
    {
        [Required]
        [StringLength(20)]
        public string Id { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string GcmId { get; set; }
    }

    /// <summary>
    /// 클라이언트로부터 로그인에 사용하는 모델입니다.
    /// 첫 로그인시 서버로부터 발급받은 토큰을 이용해 인증합니다.
    /// </summary>
    public class TokenCertViewModel
    {
        [Required]
        public string Token { get; set; }
    }

    /// <summary>
    /// 클라이언트의 로그인 요청에 대한 서버의 응답에 사용하는 모델입니다.
    /// </summary>
    public class LoginResponseModel
    {
        public string Number { get; set; }      // 학번/교번
        public string Name { get; set; }        // 이름
        public int Type { get; set; }           // 계정 타입
        public string Department { get; set; }  // 학과
        public string Token { get; set; }       // 로그인 토큰
    }
}