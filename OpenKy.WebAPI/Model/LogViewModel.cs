﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenKy.WebAPI.Model
{
    /// <summary>
    /// 로그 데이터의 한 행에 대한 데이터 집합
    /// </summary>
    public class LogViewModel
    {
        public string Facility { get; set; }    // 시설물
        public string OpenNumber { get; set; }  // 시설물을 연 사람의 학/교번
        public string OpenName { get; set; }    // 시설물을 연 사람의 이름
        public string OpenDate { get; set; }    // 시설물을 연 시간, yyyy-MM-ddHH:mm:ss
        public string CloseNumber { get; set; } // 시설물을 닫은 사람의~
        public string CloseName { get; set; }
        public string CloseDate { get; set; }
    }

    /// <summary>
    /// 로그를 요청하는 날짜에 대한 데이터 집합
    /// </summary>
    public class LogRequestViewModel : BaseViewModel
    {
        public int Facility { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
    }
}