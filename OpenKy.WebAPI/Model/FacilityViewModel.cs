﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenKy.WebAPI.Model
{
    public class BuildingViewModel
    {
        public int Index { get; set; }
        public string Name { get; set; }
    }

    public class FacilityViewModel
    {
        public int Index { get; set; }
        public int? Floor { get; set; }
        public string Name { get; set; }
    }

    public class FacilityStatusViewModel
    {
        public int Index { get; set; }
        public string Building { get; set; }
        public string Name { get; set; }
        public int State { get; set; }
    }
}