﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenKy.WebAPI.Model
{
    public class RequestUnlockViewModel
    {
        /// <summary>
        /// 태깅된 NFC Card ID
        /// </summary>
        public string Nfc { get; set; }

        /// <summary>
        /// 태그된 Device ID
        /// </summary>
        public string Device { get; set; }
    }
}