﻿using System;

namespace OpenKy.WebAPI.Model
{
    /// <summary>
    /// 신청서 뷰 모델
    /// </summary>
    public class RequestApplicationViewModel : BaseViewModel
    {
        // 이용할 시설물
        public int Facility { get; set; }

        // 이용할 날짜
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        // 이용할 교시
        public string Periods { get; set; }
        
        // 이용목적
        public string Purpose { get; set; }

        // 이용인원
        public int Peoples { get; set; }
    }

    /// <summary>
    /// 신청서 정보 뷰 모델
    /// </summary>
    public class ApplicationViewModel
    {
        public int Id { get; set; }
        public string Building { get; set; }
        public string Facility { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public string Period { get; set; }
        public int Peoples { get; set; }
        public string Purpose { get; set; }
        public int Approval { get; set; }
        public String ApplicationDate { get; set; }
    }

    /// <summary>
    /// 신청서 가불 결정 뷰 모델, 취소용으로도 씀
    /// </summary>
    public class DecideApplicationViewModel : BaseViewModel
    {
        // 결정할 신청 번호
        public int Id { get; set; }
    }
}