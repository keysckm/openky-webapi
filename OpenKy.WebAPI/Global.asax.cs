﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using OpenKy.WebAPI.Infrastructure;

namespace OpenKy.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // WebAPI 라우트 설정
            GlobalConfiguration.Configure(WebApiConfig.Register);

            // RequiredHttps 필터 Global 적용
            // GlobalConfiguration.Configuration.Filters.Add(new RequiredHttps());
        }
    }
}
