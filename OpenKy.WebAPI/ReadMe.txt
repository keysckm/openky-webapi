﻿WebAPI의 Custom HTTP Method는 다음과 같이 정의되어 있습니다.

Method		역할
LOGIN		로그인
TOKEN		로그인(TOKEN)
LOGOUT		로그아웃
UNLOCK		문 열림 요청
LOCK		문 잠금 요청

기본적인 데이터의 주고받음은 json을 이용합니다.