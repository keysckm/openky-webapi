﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace OpenKy.WebAPI.Infrastructure
{
    /// <summary>
    /// Https를 강제하기 한 Action Filter입니다.
    /// </summary>
    public class RequiredHttps : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            // Context Request의 Uri Scheme를 조사함.
            // Https Scheme가 아니라면 Context의 Response객체에 ResponseMessage를 세팅하면
            // 액션메소드로 진입하지 않고 바로 클라이언트에게 Response한다.
            if(actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);     // HTTP Code : 403 Forbidden
                actionContext.Response.ReasonPhrase = "SSL Required";                           // Http Resposne Msg : "SSL Required"
            }

            base.OnActionExecuting(actionContext);
        }
    }
}