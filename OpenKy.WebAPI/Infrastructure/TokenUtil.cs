﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenKy.WebAPI.Infrastructure
{
    public class TokenUtil
    {
        public static string GenerateToken()
        {
            var guid = Guid.NewGuid();
            return guid.ToString();
        }
    }
}