﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using System.Web.ModelBinding;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Concrete;
using OpenKy.Domain.Entities;
using OpenKy.WebAPI.Infrastructure;
using OpenKy.WebAPI.Model;

namespace OpenKy.WebAPI.Controllers
{
    public class AccountController : ApiController
    {
        private IMemberProcessor member_proc;
        private IDepartmentProcessor depart_proc;
        public AccountController(IMemberProcessor proc, IDepartmentProcessor dproc)
        {
            member_proc = proc;
            depart_proc = dproc;
        }

        [NonAction]
        private LoginResponseModel CreateResponseModel(Member m)
        {
            var d = depart_proc.GetDepartment(m.Department);
            return new LoginResponseModel()
            {
                Number = m.Number,
                Name = m.Name,
                Type = m.Type,
                Department = d.Name,
                Token = m.Token
            };
        }

        [AcceptVerbs("LOGIN")]
        [ResponseType(typeof(LoginResponseModel))]
        public IHttpActionResult Login(LoginViewModel model)    // id, pw를 이용한 인증
        {            
            if(ModelState.IsValid)
            {
                string id = model.Id;
                string pw = Util.PasswordCrypt(model.Password);

                // 전달받은 id와 pw로 member 데이터를 찾습니다.
                var result = member_proc.GetMember(id);
                if (result == null || result.Password != pw)
                    return NotFound();  // 찾을 수 없음

                // regid를 등록합니다.
                member_proc.AttachGcmId(result, model.GcmId);

                // token generate
                string token = TokenUtil.GenerateToken();
                member_proc.AttachToken(result, token);
                
                return Ok(CreateResponseModel(result));
            }
            return BadRequest();
        }

        [AcceptVerbs("TOKEN")]
        [ResponseType(typeof(LoginResponseModel))]
        public IHttpActionResult Login(TokenCertViewModel model)    // id, token을 이용한 인증
        {
            if(ModelState.IsValid)
            {
                // token string으로 model객체를 찾습니다.
                var repo = new Repository();
                var result = repo.Member.SingleOrDefault(e => e.Token == model.Token);
                if (result == null) // 찾을 수 없음 -> 토큰이 만료됨 -> 다시 로그인해야함
                    return new StatusCodeResult(HttpStatusCode.Forbidden, Request);

                return Ok(CreateResponseModel(result));
            }
            return BadRequest();
        }

        [AcceptVerbs("LOGOUT")]
        public IHttpActionResult Logout(TokenCertViewModel model)   // 로그아웃
        {
            if(ModelState.IsValid)
            {
                var result = member_proc.FindToken(model.Token);
                if (result == null) // 찾을 수 없음
                    return NotFound();

                // gcmid 해제
                member_proc.DetachGcmId(result);

                // token 무효화
                member_proc.DetachToken(result);

                return Ok();
            }

            return BadRequest();
        }
    }
}
