﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Concrete;
using OpenKy.WebAPI.Model;

namespace OpenKy.WebAPI.Controllers
{
    public class DoorController : ApiController
    {
        private IDoorProcessor door_proc;
        private IFacilityProcessor facility_proc;
        private IGcmNotification gcm_proc;

        public DoorController(IDoorProcessor proc, IFacilityProcessor fproc, IGcmNotification gproc)
        {
            door_proc = proc;
            facility_proc = fproc;
            gcm_proc = gproc;
        }

        [AcceptVerbs("UNLOCK")]
        public IHttpActionResult Unlock(RequestUnlockViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (model == null)
                return BadRequest();

            if (door_proc.Unlock(model.Nfc, model.Device, DateTime.Now))
            {
                var f = facility_proc.GetFacility(model.Device);
                if (f != null)
                {
                    // 푸시알람 데이터 생성
                    var datas = new Dictionary<string, string>
                    {
                        {"title", "시설물 상태가 변경되었습니다.."},
                        {"message", f.Name + "시설물이 열렸습니다."}
                    };
                    new GcmNotification().Push(f.Manager, datas);
                }
                return Ok();
            }

            return new StatusCodeResult(HttpStatusCode.Forbidden, Request);
        }

        [AcceptVerbs("LOCK")]
        public IHttpActionResult Lock(RequestUnlockViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (door_proc.Lock(model.Nfc, model.Device, DateTime.Now))
            {
                var f = facility_proc.GetFacility(model.Device);
                if (f != null)
                {
                    // 푸시알람 데이터 생성
                    var datas = new Dictionary<string, string>
                    {
                        {"title", "시설물 상태가 변경되었습니다.."},
                        {"message", f.Name + "시설물이 잠겼습니다."}
                    };
                    new GcmNotification().Push(f.Manager, datas);
                }
                return Ok();
            }

            return new StatusCodeResult(HttpStatusCode.Forbidden, Request);
        }
    }
}
