﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;
using OpenKy.WebAPI.Model;

namespace OpenKy.WebAPI.Controllers
{
    public class FacilityController : ApiController
    {
        private IMemberProcessor member_proc;
        private IBuildingProcessor building_proc;
        private IFacilityProcessor facility_proc;

        public FacilityController(IBuildingProcessor building_proc, IFacilityProcessor facility_proc, IMemberProcessor member_proc)
        {
            this.building_proc = building_proc;
            this.facility_proc = facility_proc;
            this.member_proc = member_proc;
        }

        private bool CheckValidate(BaseViewModel model)
        {
            var target = member_proc.GetMember(model.Number);
            return target.Token == model.Token;
        }

        // 건물에 존재하는 모든 시설물을 반환합니다.
        [HttpGet]
        [ResponseType(typeof(ICollection<FacilityViewModel>))]
        public IHttpActionResult GetFacilities(int id)   // id = building index
        {
            var building = building_proc.GetBuilding(id);
            if (building == null)
                return BadRequest();

            var facilities = facility_proc.GetFacilities(building);
            if (facilities == null)
                return NotFound();

            var items = new Collection<FacilityViewModel>();
            foreach(var f in facilities)
            {
                items.Add(new FacilityViewModel()
                {
                    Index = f.Id,
                    Floor = f.Floor,
                    Name = f.Name
                });
            }

            return Ok(items);
        }

        // 요청년월일의 사용예정을 돌려줍니다.
        [HttpGet]
        [ResponseType(typeof(ScheduleViewModel))]
        public IHttpActionResult GetFacilitySchedule(int facility, int year, int month, int day)
        {
            var f = facility_proc.GetFacility(facility);
            if (f == null)
                return NotFound();

            var s = new ScheduleViewModel()
            {
                Year = year,
                Month = month,
                Day = day,
                Period = new List<int>()
            };

            // 시설물 사용 정보를 가져온다.
            var timetables = facility_proc.GetSchedules(facility, new DateTime(year, month, day));
            if (timetables != null)
            {
                // 시간표 정보를 등록
                foreach (var period in timetables)
                    s.Period.Add(period);
            }

            return Ok(s);
        }

        [AcceptVerbs("STATE")]
        [ResponseType(typeof(ICollection<FacilityStatusViewModel>))]
        public IHttpActionResult GetFacilitiesStatus(BaseViewModel model)
        {
            if (!ModelState.IsValid || !CheckValidate(model))
                return BadRequest("인증 정보가 올바르지 않습니다.");

            var fs = facility_proc.GetFacilities().Where(e => e.Manager == model.Number);
            ICollection<FacilityStatusViewModel> views = new Collection<FacilityStatusViewModel>();
            foreach(var f in fs)
            {
                var b = building_proc.GetBuilding(f.Building);
                views.Add(new FacilityStatusViewModel()
                {
                    Index = f.Id,
                    Building = b.Name,
                    Name = f.Name,
                    State = f.IsLock == null ? 0 : f.IsLock == false ? 1 : 2
                });
            }
            return Ok(views);
        }
    }
}
