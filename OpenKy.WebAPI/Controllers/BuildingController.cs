﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Entities;
using OpenKy.WebAPI.Model;

namespace OpenKy.WebAPI.Controllers
{
    public class BuildingController : ApiController
    {
        public IBuildingProcessor building_proc;

        public BuildingController(IBuildingProcessor proc)
        {
            building_proc = proc;
        }

        [HttpGet]
        [ResponseType(typeof(ICollection<BuildingViewModel>))]
        public IHttpActionResult GetBuildings()
        {
            var buildings = building_proc.GetBuildings();
            var models = new Collection<BuildingViewModel>();
            foreach(var b in buildings)
            {
               models.Add(new BuildingViewModel()
               {
                   Index = b.Id,
                   Name = b.Name
               }); 
            }

            return Ok(models);
        }
    }
}
