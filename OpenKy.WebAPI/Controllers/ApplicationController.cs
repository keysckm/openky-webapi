﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using OpenKy.Domain.Abstract;
using OpenKy.Domain.Concrete;
using OpenKy.Domain.Entities;
using OpenKy.WebAPI.Model;

namespace OpenKy.WebAPI.Controllers
{
    public class ApplicationController : ApiController
    {
        private IMemberProcessor member_proc;
        private IUseApplicationProcessor application_proc;
        private IFacilityProcessor facility_proc;
        private IBuildingProcessor building_proc;
        private IStudentProcessor info_proc;

        public ApplicationController(IMemberProcessor mproc, IUseApplicationProcessor uproc, IFacilityProcessor fproc, IBuildingProcessor bproc)
        {
            member_proc = mproc;
            application_proc = uproc;
            facility_proc = fproc;
            building_proc = bproc;
        }

        private bool CheckValidate(BaseViewModel model)
        {
            // 인증
            var target = member_proc.GetMember(model.Number);
            return target.Token == model.Token;
        }

        // 특정 학생의 사용신청 목록 반환
        // GET /api/Application/
        [HttpGet]
        [ResponseType(typeof(ICollection<ApplicationViewModel>))]
        public IHttpActionResult GetApplications(BaseViewModel model)
        {
            if (!ModelState.IsValid || !CheckValidate(model))
                return BadRequest("인증 정보가 올바르지 않습니다.");

            // 요청자의 정보를 가져옴
            var user = member_proc.GetMember(model.Number);
            if (user == null)
                return NotFound();

            // 데이터를 가져옴
            IQueryable<UseApplication> applications = null;
            switch(user.Type)
            {
                case 1: // 학생
                    applications = application_proc.GetRequests(model.Number);
                    break;
                    
                case 3: // 교직원
                    applications = application_proc.GetRequestForManager(model.Number);
                    break;
            }
            if (applications == null || !applications.Any())
                return NotFound();

            // 도메인 모델 -> 뷰 모델
            ICollection<ApplicationViewModel> result = new Collection<ApplicationViewModel>();
            foreach(var a in applications)
            {
                var facility = facility_proc.GetFacility(a.Class);
                var building = building_proc.GetBuilding(facility.Building);
                var @new = new ApplicationViewModel()
                {
                    Id = a.Id,
                    Building = building.Name,
                    Facility = facility.Name,
                    Year = a.UseDate.Year,
                    Month = a.UseDate.Month,
                    Day = a.UseDate.Day,
                    Period = a.Period,
                    Peoples = a.Peoples,
                    Purpose = a.Purpose,
                    Approval = a.Approval == null ? 0 : a.Approval == true ? 1 : 2,
                    ApplicationDate = a.ApplicationDate.ToString("yyyy-MM-ddHH:mm:ss")
                };
                result.Add(@new);
            }
            return Ok(result.OrderByDescending(e => e.Id));
        }

        // 사용신청
        // POST /api/Application/
        [HttpPost]
        public IHttpActionResult RequestApplication(RequestApplicationViewModel model)
        {
            if (!ModelState.IsValid || !CheckValidate(model))
                return BadRequest("인증 정보가 올바르지 않습니다.");

            // ViewMode을 기반으로 UseApplication 모델을 생성합니다.
            var @new = new UseApplication()
            {
                Number = model.Number,
                Class = model.Facility,
                UseDate = new DateTime(model.Year, model.Month, model.Day),
                Period = model.Periods,
                Peoples = model.Peoples,
                Purpose = model.Purpose,
                ApplicationDate = DateTime.Now
            };

            // 데이터 등록 시도
            try
            {
                if (!application_proc.Request(@new))
                    return BadRequest("데이터베이스에 등록할 수 없었습니다.");
            }
            catch(DbEntityValidationException ex)
            {
                return BadRequest("요청 데이터가 올바르지 않습니다.");
            }

            // 시설물 정보를 가져옵니다.
            Facility f = facility_proc.GetFacility(model.Facility);
            if (f != null)
            {
                // 푸시알람 데이터 생성
                var datas = new Dictionary<string, string>
                {
                    {"title", "시설물 사용 신청이 도착했습니다."},
                    {"message", f.Name}
                };
                new GcmNotification().Push(f.Manager, datas);
            }

            return Ok();
        }

        // 사용신청 취소
        // DELETE /api/Application/
        [HttpDelete]
        public IHttpActionResult CancelApplication(DecideApplicationViewModel model)
        {
            if (!ModelState.IsValid || !CheckValidate(model))
                return BadRequest("인증 정보가 올바르지 않습니다.");

            if (!application_proc.Cancel(model.Id))
                return NotFound();
            
            return Ok();
        }

        // 사용신청 승인
        // ACCEPT /api/Application/
        [AcceptVerbs("ACCEPT")]
        public IHttpActionResult AcceptApplication(DecideApplicationViewModel model)
        {
            if (!ModelState.IsValid || !CheckValidate(model))
                return BadRequest("인증 정보가 올바르지 않습니다.");

            var result = application_proc.Accept(model.Id, model.Number);
            if (!result) 
                return new StatusCodeResult(HttpStatusCode.Forbidden, Request);
            
            // 신청자와 신청자의 담당교수 Mmeber데이터를 가져옴
            var application = application_proc.GetRequest(model.Id);
            var member = member_proc.GetMember(application.Number);
            info_proc = new StudentProc();
            var info = info_proc.GetStudent(member.Number);

            // 푸시알람을 보냄
            IGcmNotification gcm = new GcmNotification();
            var datas = new Dictionary<string, string>
            {
                {"title", "허가되었습니다."},
                {"message", "시설물 사용 신청이 허가되었습니다."}
            };
            gcm.Push(member.Number, datas);
            if(info != null)
                gcm.Push(info.Professor, "학생이 시설물을 사용합니다.");

            return Ok();
        }

        // 사용신청 거절
        // REJECT /api/Application/
        [AcceptVerbs("REJECT")]
        public IHttpActionResult RejectApplication(DecideApplicationViewModel model)
        {
            if (!ModelState.IsValid || !CheckValidate(model))
                return BadRequest("인증 정보가 올바르지 않습니다.");

            bool result = application_proc.Reject(model.Id, model.Number);
            if (result)
            {
                var application = application_proc.GetRequest(model.Id);

                // 푸시알람을 보냄
                IGcmNotification gcm = new GcmNotification();
                var datas = new Dictionary<string, string>
                {
                    {"title", "허가되었습니다."}, 
                    {"message", "시설물 사용 신청이 거부되었습니다."}
                };
                gcm.Push(application.Number, datas);

                return Ok();
            }
            return new StatusCodeResult(HttpStatusCode.Forbidden, Request);
        }
    }
}
