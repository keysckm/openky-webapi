﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Web.Http.Results;
using OpenKy.WebAPI.Model;
using OpenKy.Domain.Abstract;
using System.Web.Http;
using System.Web.Http.Description;

namespace OpenKy.WebAPI.Controllers
{
    public class LogController : ApiController
    {
        private ILogProcessor log_proc;
        private IMemberProcessor mem_proc;
        private IFacilityProcessor fac_proc;

        public LogController(ILogProcessor lproc, IMemberProcessor mproc, IFacilityProcessor fproc)
        {
            log_proc = lproc;
            mem_proc = mproc;
            fac_proc = fproc;
        }

        private bool CheckValidate(BaseViewModel model)
        {
            // 인증
            var target = mem_proc.GetMember(model.Number);
            return target.Token == model.Token;
        }

        // GET: /api/Log
        // RequestBody: LogRequestViewModel
        [HttpGet]
        [ResponseType(typeof(ICollection<LogViewModel>))]
        public IHttpActionResult GetLog(LogRequestViewModel model)
        {
            if (!ModelState.IsValid || !CheckValidate(model))
                return BadRequest("인증 정보가 올바르지 않습니다.");

            var logs = log_proc.GetLogs(model.Facility, new DateTime(model.Year, model.Month, model.Day));
            ICollection<LogViewModel> views = new Collection<LogViewModel>();
            foreach(var log in logs)
            {
                var open_member_name = mem_proc.GetMember(log.UnlockUser).Name;
                string close_member_name = log.UnlockUser == log.LockUser ? open_member_name : mem_proc.GetMember(log.LockUser).Name;
                var m = new LogViewModel()
                {
                    Facility = fac_proc.GetFacility(model.Facility).Name,
                    OpenNumber = log.UnlockUser,
                    OpenName = open_member_name,
                    OpenDate = log.UnlockDate.ToString("yyyy-MM-ddHH:mm:ss"),
                    CloseNumber = log.LockUser,
                    CloseName = close_member_name,
                    CloseDate = log.LockDate.Value.ToString("yyyy-MM-ddHH:mm:ss")
                };
                views.Add(m);
            }

            return Ok(views);
        }
    }
}
